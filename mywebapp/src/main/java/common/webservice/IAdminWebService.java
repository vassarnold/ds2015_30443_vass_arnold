package common.webservice;

import common.entity.City;
import common.entity.User;
import common.vo.PackageVO;
import common.vo.StatusVO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by Arnold on 17/01/16.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IAdminWebService {
    @WebMethod
    public User login (String username, String password);

    @WebMethod
    public User[] getAllUsers ();

    @WebMethod
    public City[] getAllCities ();

    @WebMethod
    public common.entity.Package[] getAllPackages();

    @WebMethod
    public void savePackage(PackageVO packagee);

    @WebMethod
    public void deletePackage (int id);

    @WebMethod
    public void trackPackage (int id);

    @WebMethod
    public void updateStatus (StatusVO statusVO);

    @WebMethod
    public void register (String username, String password);
}
