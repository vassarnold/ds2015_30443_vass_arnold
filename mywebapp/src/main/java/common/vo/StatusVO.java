package common.vo;

import java.util.Date;

/**
 * Created by Arnold on 17/01/16.
 */
public class StatusVO {
    private int packagee;
    private int city;
    private Date date;

    public int getPackagee() {
        return packagee;
    }

    public void setPackagee(int packagee) {
        this.packagee = packagee;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
