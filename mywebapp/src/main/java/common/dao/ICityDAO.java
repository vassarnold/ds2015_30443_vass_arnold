package common.dao;

import common.entity.City;

import java.util.List;

/**
 * Created by Arnold on 17/01/16.
 */
public interface ICityDAO {
    public List<City> getAll();

    public City load (int id);
}
