package common.dao;

import common.entity.Route;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Arnold on 17/01/16.
 */
public class RouteDAO implements IRouteDAO {
    private SessionFactory factory = MySessionFactory.getSessionFactory();

    @Override
    public void persist(Route route) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(route);
        tx.commit();
        session.flush();
        session.close();
    }

    @Override
    public List<Route> getRoutesForPackage(int packageId) {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(Route.class);
        criteria.createCriteria("packagee").add(Restrictions.eq("id", packageId));
        List<Route> routes = criteria.list();
        session.close();
        return routes;
    }
}
