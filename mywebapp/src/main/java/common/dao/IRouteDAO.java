package common.dao;

import common.entity.Route;

import java.util.List;

/**
 * Created by Arnold on 17/01/16.
 */
public interface IRouteDAO {
    public void persist (Route route);

    public List<Route> getRoutesForPackage (int packageId);
}
