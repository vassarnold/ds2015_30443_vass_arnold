package common.dao;

import common.entity.User;

import java.util.List;

/**
 * Created by Arnold on 17/01/16.
 */
public interface IUserDAO {
    public User login (String username, String password);

    public List<User> getAll ();

    public User load (int id);

    public void register (String username, String password);
}
