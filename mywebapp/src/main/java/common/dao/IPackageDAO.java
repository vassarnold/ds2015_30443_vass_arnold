package common.dao;

import common.entity.*;
import common.entity.Package;

import java.util.List;

/**
 * Created by Arnold on 17/01/16.
 */
public interface IPackageDAO {
    public List<Package> getAll();

    public void persist (Package packagee);

    public void delete (int id);

    public void track (int id);

    public Package load (int id);

    public List<Package> getPackagesForUser (int id);

    public List<Package> searchPackage (String searchString);
}
