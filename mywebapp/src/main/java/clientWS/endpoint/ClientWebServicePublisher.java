package clientWS.endpoint;

import clientWS.webservice.ClientWebService;

import javax.xml.ws.Endpoint;

/**
 * Created by Arnold on 17/01/16.
 */
public class ClientWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8085/clientwebservice/servlet", new ClientWebService());
    }
}
