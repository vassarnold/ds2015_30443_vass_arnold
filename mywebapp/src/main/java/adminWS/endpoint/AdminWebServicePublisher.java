package adminWS.endpoint;

import adminWS.webservice.AdminWebService;

import javax.xml.ws.Endpoint;

/**
 * Created by Arnold on 17/01/16.
 */
public class AdminWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8086/adminwebservice/admin", new AdminWebService());
    }

}
