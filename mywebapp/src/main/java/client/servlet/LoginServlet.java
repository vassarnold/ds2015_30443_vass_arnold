package client.servlet;

import client.webservice.ServiceLocator;
import common.entity.*;
import common.entity.Package;
import common.webservice.IAdminWebService;
import common.webservice.IClientWebService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Arnold on 17/01/16.
 */
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");
        String action = httpServletRequest.getParameter("action");
        if (action.equals("login")) {
            User loginUser = ServiceLocator.getIAdminWebService().login(username, password);
            httpServletRequest.getSession(true).setAttribute("userId", loginUser.getId());
            if (loginUser.getUsername() == null) {
                httpServletRequest.setAttribute("loginResult", false);
                httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest, httpServletResponse);
            } else if (loginUser.getIsAdmin()) {    //admin
                IAdminWebService IAdminWebService = ServiceLocator.getIAdminWebService();
                User[] users = IAdminWebService.getAllUsers();
                City[] cities = IAdminWebService.getAllCities();
                common.entity.Package[] packages = IAdminWebService.getAllPackages();
                httpServletRequest.setAttribute("users", users);
                httpServletRequest.setAttribute("cities", cities);
                httpServletRequest.setAttribute("packages", packages);
                httpServletRequest.getRequestDispatcher("/admin.jsp").forward(httpServletRequest, httpServletResponse);
            } else {    //servlet
                IClientWebService IClientWebService = ServiceLocator.getIClientWebService();
                Package[] packages = IClientWebService.getOwnPackages(loginUser.getId());
                httpServletRequest.setAttribute("packages", packages);
                httpServletRequest.getRequestDispatcher("/servlet.jsp").forward(httpServletRequest, httpServletResponse);
            }
        }
        else if (action.equals("register")) {
            ServiceLocator.getIAdminWebService().register(username, password);
            httpServletRequest.setAttribute("registerResult", true);
            httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest, httpServletResponse);

        }


    }
}
