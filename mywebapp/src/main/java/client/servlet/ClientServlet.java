package client.servlet;

import client.webservice.ServiceLocator;
import common.entity.*;
import common.webservice.IClientWebService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Arnold on 17/01/16.
 */
public class ClientServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action.equals("search")) {
            common.entity.Package[] searchResult = ServiceLocator.getIClientWebService().getSearchResults(request.getParameter("searchName"));
            request.setAttribute("searchResults", searchResult);
        } else if (action.equals("checkStatus")) {
            Route[] routes = ServiceLocator.getIClientWebService().getRoutesForPackage(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("routes", routes);
        }
        forward(request, response);
    }

    private void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        IClientWebService IClientWebService = ServiceLocator.getIClientWebService();
        common.entity.Package[] packages = IClientWebService.getOwnPackages((int) request.getSession().getAttribute("userId"));
        request.setAttribute("packages", packages);
        request.getRequestDispatcher("/servlet.jsp").forward(request, response);
    }
}
