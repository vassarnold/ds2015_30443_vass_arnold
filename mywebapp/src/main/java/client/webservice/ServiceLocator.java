package client.webservice;

import common.webservice.IAdminWebService;
import common.webservice.IClientWebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Arnold on 17/01/16.
 */
public class ServiceLocator {
    private static IAdminWebService IAdminWebService = null;
    private static IClientWebService IClientWebService = null;

    public static IAdminWebService getIAdminWebService() {
        if (IAdminWebService == null) {
            URL wsdlUrl = null;
            try {
                wsdlUrl = new URL("http://localhost:8086/adminwebservice/admin?wsdl");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            QName qname = new QName("http://webservice.adminWS/", "AdminWebServiceService");
            Service service = Service.create(wsdlUrl, qname);
            IAdminWebService = service.getPort(IAdminWebService.class);
        }
        return IAdminWebService;
    }
    public static IClientWebService getIClientWebService() {
        if (IClientWebService == null) {
            URL wsdlUrl = null;
            try {
                wsdlUrl = new URL("http://localhost:8085/clientwebservice/servlet?wsdl");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            QName qname = new QName("http://webservice.clientWS/", "ClientWebServiceService");
            Service service = Service.create(wsdlUrl, qname);
            IClientWebService = service.getPort(IClientWebService.class);
        }
        return IClientWebService;
    }
}
