<%--
  Created by IntelliJ IDEA.
  User: Arnold
  Date: 17/01/16
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.getSession(false).invalidate(); %>
<html>
<head>
    <title></title>
</head>
<body>
<h2>Login</h2>

<form action="login" method="POST">
    <input type="hidden" name="action" value="login">
    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
    </table>
    <input type="submit" value="Log in">
</form>

<% if (request.getAttribute("loginResult") != null){
    if ((Boolean)request.getAttribute("loginResult") == true){
        out.println("Success.");
    }
    else{
        out.print("Incorrect username or password.");
    }
} %>

<h2>Register</h2>

<form action="login" method="POST">
    <input type="hidden" name="action" value="register">

    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
    </table>
    <input type="submit" value="Register">
</form>

<% if (request.getAttribute("registerResult") != null){
    if ((Boolean)request.getAttribute("registerResult") == true){
        out.println("Success.");
    }
    else{
        out.print("Error");
    }
} %>

</body>
</html>
