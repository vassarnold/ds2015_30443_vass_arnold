package dao;

import entities.Flight;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.swing.*;
import java.util.List;

/**
 * Created by Arnold on  25/10/2015.
 */
public class FlightDAO {
    private SessionFactory factory;
    private Session savedSession;

    public FlightDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight[] getFlightList(){
        Session session = factory.openSession();
        try {
            List flightList = session.createQuery("from Flight").list();
            Flight[] flights = new Flight[flightList.size()];
            for (int i = 0; i < flightList.size(); i++) {
                flights[i] = (Flight) flightList.get(i);
            }
            return flights;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error in getFlightsList():" + e.toString());
            return null;
        }
    }
    public  void insertFlight(Flight flight){
        Session session;
        if (savedSession != null) {
            session = savedSession;
        }else{
            session = factory.openSession();
        }
        try{
            Transaction transaction = session.beginTransaction();
            session.save(flight);
            transaction.commit();
        }catch(Exception e){
            System.out.println("Error while inserting flight into database");
        }finally{
            session.flush();
            session.close();
            savedSession = null;
        }
    }
    public void update(int flightNr){
        try{
            Session session = factory.openSession();
            Transaction tr = session.getTransaction();
            tr.begin();
            Flight flight = (Flight)session.load(Flight.class,flightNr);
            JOptionPane.showMessageDialog(null,flight.toString());
            session.saveOrUpdate(flight);
            tr.commit();
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error when updating flight");
        }
    }
    public int getNrOfFlights(){
        Session session = factory.openSession();
        try {
            List flightsList = session.createQuery("from Flight").list();

            return flightsList.size();
        } catch (Exception ex) {
            return -1;
        }
    }
    public String getLocalTimes(int i){
        Session session = factory.openSession();
        try {
            List flightsList = session.createQuery("from Flight").list();
            Flight Flight = (Flight) flightsList.get(i);
            return "This flight departures at: " + Flight.getDateDeparture() + "local time,\nAnd arrives at: " + Flight.getDateArrival() + " local time";
        } catch (Exception e) {
            return null;
        }
    }
    public void deleteFlight(int id) {
        try {
            Session session = factory.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Flight where idFlight = :idFlight");
            query.setParameter("idFlight", id);
            query.executeUpdate();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }
    public Flight getFlightAtPosition(int pos){
        Flight[] flights = getFlightList();
        return flights[pos];
    }
    public Flight load(int id){
        Session session = factory.openSession();
        savedSession = session;
        return (Flight)session.load(Flight.class,id);
    }
}
