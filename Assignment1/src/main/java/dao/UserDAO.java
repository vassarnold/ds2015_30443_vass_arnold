package dao;


import java.util.List;

import entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
/**
 * Created by Arnold on 25/10/2015.
 */
public class UserDAO {
    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    private SessionFactory factory;

    public UserDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public String validate(String name, String password) {
       Session session = factory.openSession();
        try{
            List users = session.createQuery("from User").list();
            for (Object user :users){
                User currentUser = (User)user;
                if (currentUser.getUsername().equals(name) &&currentUser.getPassword().equals(password)){
                    return currentUser.getPriviledge();
                }
            }
        }catch (Exception e){
            System.out.println("Exception"+e.toString());
            return null;
        }
        return  null;
    }

}
