package dao;

import entities.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Arnold on  25/10/2015.
 */
public class CityDAO {
    private SessionFactory factory;


    public CityDAO(SessionFactory factory) {
        this.factory = factory;
    }
    public City[] getCityList(){
        Session session = factory.openSession();
        try {
            // get all cities into a list first
            List citiesList = session.createQuery("from City").list();
            // then move Cities from the list to an array[]
            City[] cities = new City[citiesList.size()];
            for (int i = 0; i < citiesList.size(); i++) {
                cities[i] = (City)citiesList.get(i);
            }
            return cities;
        } catch (Exception ex) {
            return null;
        }
    }
    public City get(int id) {
        Session session = factory.openSession();

        return (City)session.load(City.class, id);
    }
}
