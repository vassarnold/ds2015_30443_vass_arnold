package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by Arnold on  25/15/2015.
 */
public class UpdateFlightServlet extends HttpServlet {
    FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        if (httpServletRequest.getParameter("the_action") != null && httpServletRequest.getParameter("the_action").equals("update_the_flight")) {
            try {
                int updatedFlightId = Integer.parseInt(httpServletRequest.getParameter("idFlight"));

                Flight modified_flight = flightDAO.load(updatedFlightId);


                // formatter for times
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                // set attributes for the flight
                modified_flight.setDateDeparture(format.parse(httpServletRequest.getParameter("dateDeparture")));
                modified_flight.setDateArrival(format.parse(httpServletRequest.getParameter("dateArrival")));
                modified_flight.setIdDeparture(Integer.parseInt(httpServletRequest.getParameter("idDeparture")));
                modified_flight.setIdArrival(Integer.parseInt(httpServletRequest.getParameter("idArrival")));
                modified_flight.setAirplaneType(httpServletRequest.getParameter("airplaneType"));
                flightDAO.update(modified_flight.getIdFlight());

                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/admin.jsp");
                requestDispatcher.forward(httpServletRequest, httpServletResponse);

            } catch (Exception ex) {

            }

        } else {

            FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
            CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());

            Flight updated_flight = flightDAO.load(Integer.parseInt(httpServletRequest.getParameter("update_id")));
            httpServletRequest.setAttribute("idDeparture", cityDAO.get(updated_flight.getIdDeparture()).getName());
            httpServletRequest.setAttribute("idArrival", cityDAO.get(updated_flight.getIdArrival()).getName());
            httpServletRequest.setAttribute("dateDeparture", updated_flight.getDateDeparture());
            httpServletRequest.setAttribute("dateArrival", updated_flight.getDateArrival());
            httpServletRequest.setAttribute("airplaneType", updated_flight.getAirplaneType());
            httpServletRequest.setAttribute("idFlight", updated_flight.getIdFlight());

            RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/update.jsp");
            requestDispatcher.forward(httpServletRequest, httpServletResponse);
        }
    }

}
