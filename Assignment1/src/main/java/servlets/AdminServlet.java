package servlets;

import dao.FlightDAO;

import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by Arnold on  25/11/2015.
 */
public class AdminServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        try{
            String action = httpServletRequest.getParameter("action");
            if(action.equals("delete")) {
                int id = Integer.parseInt(httpServletRequest.getParameter("id"));

                FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
                flightDAO.deleteFlight(id);

                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/admin.jsp");
                requestDispatcher.forward(httpServletRequest, httpServletResponse);
            }
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, e.toString());
        }

    }
}
