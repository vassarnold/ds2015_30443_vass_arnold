package servlets;

import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by Arnold on  25/14/2015.
 */

public class CreateFlight extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        Flight flight = new Flight();
        flight.setAirplaneType(httpServletRequest.getParameter("airplaneType"));
        flight.setDateArrival(formatter.parseDateTime(httpServletRequest.getParameter("dateArrival")).toDate());
        flight.setDateDeparture((formatter.parseDateTime(httpServletRequest.getParameter("dateDeparture")).toDate()));
        flight.setIdDeparture(Integer.parseInt(httpServletRequest.getParameter("idDeparture")));
        flight.setIdArrival(Integer.parseInt(httpServletRequest.getParameter("idArrival")));
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        flightDAO.insertFlight(flight);

        RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/admin.jsp");
        requestDispatcher.forward(httpServletRequest, httpServletResponse);
    }
}
