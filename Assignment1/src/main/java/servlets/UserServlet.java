package servlets;

import dao.FlightDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Arnold on  25/11/2015.
 */
public class UserServlet extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());

        int noOfFlights = flightDAO.getNrOfFlights();
        String buttonName;
        for (int i = 0; i < noOfFlights; i++) {
            buttonName = "button" + i;
            if (httpServletRequest.getParameter(buttonName) != null) {
                httpServletResponse.getWriter().print(flightDAO.getLocalTimes(i));
            }
        }
    }
}
