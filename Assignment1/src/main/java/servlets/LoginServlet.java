package servlets;

import dao.UserDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by Arnold on  25/10/2015.
 */
public class LoginServlet extends HttpServlet {


    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String username = httpServletRequest.getParameter("Username");
        String password = httpServletRequest.getParameter("Password");

        UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
        String loginResult = userDAO.validate(username,password);
        if (loginResult.equals("admin")){
            RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/admin.jsp");
            requestDispatcher.forward(httpServletRequest,httpServletResponse);
        }else {
            if (loginResult.equals("user")) {
                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/user.jsp");
                requestDispatcher.forward(httpServletRequest, httpServletResponse);
            } else {
                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/other.jsp");
                requestDispatcher.forward(httpServletRequest, httpServletResponse);
            }
        }
    }

    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doGet(httpServletRequest, httpServletResponse);
    }
}
