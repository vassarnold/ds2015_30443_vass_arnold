package entities;

/**
 * Created by Arnold on 25/10/2015.
 */
public class User {

    private int idUser;
    private String Username;
    private String Password;
    private String Priviledge;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPriviledge() {
        return Priviledge;
    }

    public void setPriviledge(String priviledge) {
        Priviledge = priviledge;
    }

    public String toString() {
        return "User{" +
                "Username='" + Username + '\'' +
                ", Password='" + Password + '\'' +
                ", Priviledge=" + Priviledge +
                '}';
    }
}
