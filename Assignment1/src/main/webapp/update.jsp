<%@ page isELIgnored="false" %>
<%@ page import="org.hibernate.cfg.Configuration"%>

<%@ page import="dao.FlightDAO"%>
<%@ page import="dao.CityDAO"%>

<%@ page import="entities.City"%>
<%@ page import="entities.Flight"%>

<html>
    <body>

    <h2>Update GUI</h2>

    <b>Change the fields that you intend to!</b>
    <br>

    <br>
    <form action="update" method="POST">
        <table>
            <tr>
                <td>Departure City:</td>
                <td><input type="text" name="idDeparture" value=${idDeparture}></td>

            </tr>
            <tr>
                <td>Arrival City:</td>
                <td><input type="text" name="idArrival" value=${idArrival}></td>
            </tr>
            <tr>
                <td>Departure Time:</td>
                <td><input type="datetime" name="dateDeparture" value=${dateDeparture}/></td>
            </tr>
            <tr>
                <td>Arrival Time:</td>
                <td><input type="datetime" name="dateArrival" value=${dateArrival}/></td>
            </tr>
            <tr>
                <td>Airplane type:</td>
                <td><input type="text" name="airplaneType" value=${airplaneType}></td>
            <tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update"/></td>
        </table>
        <input type="hidden" name="idFlight" value=${idFlight}/>
        <input type="hidden" name="the_action" value="update_the_flight"/>
    </form>

    </body>
</html>
