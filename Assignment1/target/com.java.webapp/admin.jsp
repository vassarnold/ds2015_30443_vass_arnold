<%@ page import="org.hibernate.cfg.Configuration"%>

<%@ page import="dao.FlightDAO"%>
<%@ page import="dao.CityDAO"%>

<%@ page import="entities.City"%>
<%@ page import="entities.Flight"%>

<html>
<body>
<h2>Admin GUI</h2>
<b>List of Flights</b>
<br>

<table border="1" style="border-collapse: collapse;">
<tr>
    <td>Flight No.</td>
    <td>Departure City</td>
    <td>Arrival City</td>
    <td>Departure Date</td>
    <td>Arrival Date</td>
    <td>Airplane Type</td>
</tr>
<%
// get the array of flights
FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
Flight[] flights = flightDAO.getFlightList();

// get the array of cities
CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
City[] cities = cityDAO.getCityList();

String buttonName = "";
int dep = 0, arr = 0;
for (int i = 0; i < flights.length; i++) {
    // create the name of the button
    buttonName = "button" + i;

    // calculate the indexes of cities of departure and arrival
    for (int j = 0; j < cities.length; j++) {
        if (cities[j].getIdCity() == flights[i].getIdDeparture()) {
            dep = j;
        }
        if (cities[j].getIdCity() == flights[i].getIdArrival()) {
            arr = j;
        }
    }
    %>
    <tr>
        <td><%=flights[i].getIdFlight()%></td>
        <td><%=cities[dep].getName()%></td>
        <td><%=cities[arr].getName()%></td>
        <td><%=flights[i].getDateDeparture()%></td>
        <td><%=flights[i].getDateArrival()%></td>
        <td><%=flights[i].getAirplaneType()%></td>
        <td>
            <form action="user" method="POST">
                <input type="submit" name=<%=buttonName%> value="Local Times"/>
            </form>
        </td>
        <td>
            <a href= <%="/com.java.webapp/admin?id=" + flights[i].getIdFlight() + "&action=delete"%> >Delete</a>
        </td>
        <td>
            <form action="update" method="POST">
                <input type="hidden" name="update_id" value="<%out.print(flights[i].getIdFlight());%>">
                <input type="submit" name="" value="Update"/>
            </form>
        </td>
    <tr>
    <%
}
%>
</table>

<form action="createFlight.jsp" method="POST">
    <input type="submit" value="Create"/>
</form>

</body>
</html>
