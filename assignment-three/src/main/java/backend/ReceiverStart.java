package backend;

import com.rabbitmq.client.*;
import common.DVD;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Arnold on 13/12/15.
 */
public class ReceiverStart {
    public static final String QUEUE_NAME = "muviz_q";

    public static void main(String[] args) throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");

        Connection connection = connectionFactory.newConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        System.out.printf("Waiting for messages... Ctrl+C to stop...");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.printf("Received message: '" + message + "'");

                DVD newDVD = createDVDObject(message);

                if (newDVD != null) {
                    // create text file
                    TextFileCreator.createTextFile(newDVD);
                    // send mails
                    MailSender.sendMail(newDVD);
                }

            }

            public DVD createDVDObject(String dvd) {
                String title = "";
                String year = "";
                String price = "";

                int i, j, k;

                for (i = 0; i < dvd.length() && dvd.charAt(i) != '\n'; i++);
                for (j = i; j > 0 && dvd.charAt(j) != '-'; j--);
                title = dvd.substring(j+2, i);

                for (k = i+1; k < dvd.length() && dvd.charAt(k) != '\n'; k++);
                for (j = k; j > 0 && dvd.charAt(j) != '-'; j--);
                year = dvd.substring(j+2, k);

                i = dvd.length()-1;
                for (j = i; j > 0 && dvd.charAt(j) != '-'; j--);
                price = dvd.substring(j+2, i+1);

                try {
                    //System.out.println("Title:" + title + "\nYear:" + year + "\nPrice:" + price);
                    DVD newDVD = new DVD(title, Integer.parseInt(year), Double.parseDouble(price));
                    return newDVD;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.toString());
                    return null;
                }
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
