package main;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import common.DVD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Arnold on 13/12/15.
 */
public class InsertServlet  extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // TODO get stuff from the form, validate it, create new DVD, send it to RabbitMQ
        String titleString = request.getParameter("dvd_title");
        String yearString = request.getParameter("dvd_year");
        String priceString = request.getParameter("dvd_price");

        try{
            int year = Integer.parseInt(yearString);
            double price = Double.parseDouble(priceString);

            // create a new DVD object
            DVD newDVD = new DVD(titleString, year, price);

            // send the newly created object into the RabbitMQ queue
            sendIntoQueue(newDVD);

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/succesfull-insert.jsp");
            requestDispatcher.forward(request, response);

        } catch (Exception ex) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/denied-insert.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    private void sendIntoQueue(DVD dvd) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare("muviz_q", false, false, false, null);

        channel.basicPublish("", "muviz_q", null, (dvd.toString()).getBytes());

        channel.close();
        connection.close();
    }
}
