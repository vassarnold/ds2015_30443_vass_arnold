'use strict';

App.controller('CarController', ['$scope', 'CarService', function($scope, CarService) {
          var self = this;
          self.car={id:null,brand:'',price:'',year:''};
          self.cars=[];
              
          self.fetchAllCars = function(){
              CarService.fetchAllCars()
                  .then(
      					       function(d) {
      						        self.cars = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createCar = function(car){
              CarService.createCar(car)
		              .then(
                      self.fetchAllCars, 
				              function(errResponse){
					               console.error('Error while creating Car.');
				              }	
                  );
          };

         self.updateCar = function(car, id){
              CarService.updateCar(car, id)
		              .then(
				              self.fetchAllCars, 
				              function(errResponse){
					               console.error('Error while updating Car.');
				              }	
                  );
          };

         self.deleteCar = function(id){
              CarService.deleteCar(id)
		              .then(
				              self.fetchAllCars, 
				              function(errResponse){
					               console.error('Error while deleting Car.');
				              }	
                  );
          };

          self.fetchAllCars();

          self.submit = function() {
              if(self.car.id==null){
                  console.log('Saving New Car', self.car);    
                  self.createCar(self.car);
              }else{
                  self.updateCar(self.car, self.car.id);
                  console.log('Car updated with id ', self.car.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.cars.length; i++){
                  if(self.cars[i].id == id) {
                     self.car = angular.copy(self.cars[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              for(var i = 0; i < self.cars.length; i++){
                  if(self.cars[i].id == id) {
                     self.reset();
                     break;
                  }
              }
              self.deleteCar(id);
          };

          
          self.reset = function(){
              self.Car={id:null,brand:'',price:'',year:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
