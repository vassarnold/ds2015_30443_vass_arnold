<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>AngularJS $http Example</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }

        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }

        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }

        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }

        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp">
<div class="generic-container" ng-controller="RentalController as ctrl">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Rental Management Form </span></div>
        <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                <input type="hidden" ng-model="ctrl.rental.id"/>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="uname">Name</label>

                        <div class="col-md-7">
                        	<select ng-model = "choice">
                        		<option ng-repeat="u in ctrl.users" value="{{u.id}}" ng-selected ="{{ctrl.user.id==u.id}}">{{u.name}}</option>
                        	</select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="ucar">Car</label>
						<div class="col-md-7">
                        	<select ng-model = "choice">
                        		<option ng-repeat="c in cars"  value="{{c.id}}" ng-selected ="{{ctrl.car.id==c.id}}">{{c.brand}}</option>
                        	</select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="sdate">Start Date</label>

                        <div class="col-md-7">
                            <input type="date" ng-model="ctrl.rental.sdate"  value="{{ date | date: 'yyyy-MM-dd' }}" id="sdate"
                                   class="sdate form-control input-sm" placeholder="Enter starting date"
                                   required/>

                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.sdate.$error.required">This is a required field</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="edate">End Date</label>

                        <div class="col-md-7">
                            <input type="date" ng-model="ctrl.rental.edate"  value="{{ date | date: 'yyyy-MM-dd' }}" id="edate"
                                   class="edate form-control input-sm" placeholder="Enter end date"
                                   required/>

                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.edate.$error.required">This is a required field</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit" value="{{!ctrl.user.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm"
                                ng-disabled="myForm.$pristine">Reset Form
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Rentals </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID.</th>
                    <th>Name</th>
                    <th>Brand</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="r in ctrl.rentals">
                    <td><span ng-bind="r.id"></span></td>
                    <td><span ng-bind="r.user.name"></span></td>
                    <td><span ng-bind="r.car.brand"></span></td>
                    <td><span ng-bind="r.sdate"></span></td>
                    <td><span ng-bind="r.edate"></span></td>
                    <td>
                        <button type="button" ng-click="ctrl.remove(r.id)" class="btn btn-danger custom-width">Remove
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<c:url value='/static/classes/angular.js' />"></script>
<script src="<c:url value='/static/classes/angular-route.js' />"></script>
<script src="<c:url value='/static/classes/app.js' />"></script>
<script src="<c:url value='/static/classes/rental_service.js' />"></script>
<script src="<c:url value='/static/classes/rental_controller.js' />"></script>
</body>
</html>