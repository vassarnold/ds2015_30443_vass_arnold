package com.arnold.dsfinal.services;

import java.util.List;

import com.arnold.dsfinal.entities.Rental;

public interface IRentalService {

	Rental findById(int id);
	
	void saveRental(Rental rental);
	
	void deleteRental(int id);
	
	List<Rental> findAllRentals();
}
