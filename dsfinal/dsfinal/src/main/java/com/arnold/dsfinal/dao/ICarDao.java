package com.arnold.dsfinal.dao;

import java.util.List;

import com.arnold.dsfinal.entities.Car;
import com.arnold.dsfinal.entities.User;

public interface ICarDao {
	
	Car findById(int id);

	void saveCar(Car car);
	
	void deleteCar(int id);
	
	List<Car> findAllCars(); 

	Car findCarByBrand(String brand);

}
