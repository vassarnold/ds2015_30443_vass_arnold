package com.arnold.dsfinal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.arnold.dsfinal.entities.Rental;

@Repository("rentalDao")
public class RentalDao extends AbstractDao<Integer, Rental> implements IRentalDao {

	@Override
	public void saveRental(Rental rental) {
		persist(rental);
		
	}

	@Override
	public void deleteRental(int id) {
		Query query = getSession().createSQLQuery("delete from rental where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rental> findAllRentals() {
		Criteria criteria = createEntityCriteria();
		return (List<Rental>) criteria.list();
	}

	@Override
	public Rental findById(int id) {
		return getByKey(id);
	}
	

}
