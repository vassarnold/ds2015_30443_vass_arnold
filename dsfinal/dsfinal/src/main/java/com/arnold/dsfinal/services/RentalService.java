package com.arnold.dsfinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arnold.dsfinal.dao.RentalDao;
import com.arnold.dsfinal.entities.Rental;
@Service("rentalService")
@Transactional
public class RentalService implements IRentalService {
	
	@Autowired
	RentalDao dao;
	
	@Override
	public Rental findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveRental(Rental rental) {
		dao.saveRental(rental);
		
	}

	@Override
	public void deleteRental(int id) {
		dao.deleteRental(id);
		
	}

	@Override
	public List<Rental> findAllRentals() {
		return dao.findAllRentals();
	}

	
}
