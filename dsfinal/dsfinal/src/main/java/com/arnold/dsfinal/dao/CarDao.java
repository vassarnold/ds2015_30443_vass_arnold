package com.arnold.dsfinal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.arnold.dsfinal.entities.Car;
import com.arnold.dsfinal.entities.User;

@Repository("carDao")
public class CarDao extends AbstractDao<Integer,Car> implements ICarDao {

	@Override
	public Car findById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveCar(Car car) {
		persist(car);
		
	}

	@Override
	public void deleteCar(int id) {
		Query query = getSession().createSQLQuery("delete from car where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Car> findAllCars() {
		Criteria criteria = createEntityCriteria();
		return (List<Car>) criteria.list();
	}

	@Override
	public Car findCarByBrand(String brand) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("brand", brand));
		return (Car) criteria.uniqueResult();
	}

}
