package com.arnold.dsfinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.arnold.dsfinal.entities.Car;
import com.arnold.dsfinal.services.ICarService;

@Controller
public class CarController {

	@Autowired
    ICarService carService;  //Service which will do all data retrieval/manipulation work
	//-------------------Retrieve All Cars--------------------------------------------------------
    
    @RequestMapping(value = "/car/", method = RequestMethod.GET)
    public ResponseEntity<List<Car>> listAllCars() {
        List<Car> cars = carService.findAllCars();
        if(cars.isEmpty()){
            return new ResponseEntity<List<Car>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Car>>(cars, HttpStatus.OK);
    }
  //-------------------Create a Car--------------------------------------------------------
    
    @RequestMapping(value = "/car/", method = RequestMethod.POST)
    public ResponseEntity<Void> createCar(@RequestBody Car car,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Car " + car.getBrand());
  
        if (carService.findCarByBrand(car.getBrand())!=null) {
            System.out.println("A Car with name " + car.getBrand() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
  
        carService.saveCar(car);
  
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/car/{id}").buildAndExpand(car.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
  //------------------- Update a Car --------------------------------------------------------
    
    @RequestMapping(value = "/car/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Car> updateCar(@PathVariable("id") int id, @RequestBody Car car) {
        System.out.println("Updating Car " + id);
          
        Car currentCar = carService.findById(id);
          
        if (currentCar==null) {
            System.out.println("Car with id " + id + " not found");
            return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
        }
  
        currentCar.setBrand(car.getBrand());
        currentCar.setPrice(car.getPrice());
        currentCar.setYear(car.getYear());
          
        carService.updateCar(currentCar);
        return new ResponseEntity<Car>(currentCar, HttpStatus.OK);
    }
  
     
     
    //------------------- Delete a Car --------------------------------------------------------
      
    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Car> deleteCar(@PathVariable("id") int id) {
        System.out.println("Fetching & Deleting Car with id " + id);
  
        Car car = carService.findById(id);
        if (car == null) {
            System.out.println("Unable to delete. Car with id " + id + " not found");
            return new ResponseEntity<Car>(HttpStatus.NOT_FOUND);
        }
  
        carService.deleteCar(id);
        return new ResponseEntity<Car>(HttpStatus.NO_CONTENT);
    }
	
}
