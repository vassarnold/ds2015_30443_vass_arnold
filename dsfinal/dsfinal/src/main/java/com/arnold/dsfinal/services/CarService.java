package com.arnold.dsfinal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arnold.dsfinal.dao.CarDao;
import com.arnold.dsfinal.dao.UserDao;
import com.arnold.dsfinal.entities.Car;
import com.arnold.dsfinal.entities.User;

@Service("carService")
@Transactional
public class CarService implements ICarService {

	@Autowired
	private CarDao dao;
	@Override
	public Car findById(int id) {
		
		return dao.findById(id);
	}

	@Override
	public void saveCar(Car car) {
		dao.saveCar(car);
		
	}

	@Override
	public void updateCar(Car car) {
		Car entity = dao.findById(car.getId());
		if(entity!=null){
			entity.setBrand(car.getBrand());
			entity.setPrice(car.getPrice());
			entity.setYear(car.getYear());
		}
		
	}

	@Override
	public void deleteCar(int id) {
		dao.deleteCar(id);
		
	}

	@Override
	public List<Car> findAllCars() {
		// TODO Auto-generated method stub
		return dao.findAllCars();
	}

	@Override
	public Car findCarByBrand(String brand) {
		
		return dao.findCarByBrand(brand);
	}

}
