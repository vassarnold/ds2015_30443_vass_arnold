package com.arnold.dsfinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.arnold.dsfinal.entities.Car;
import com.arnold.dsfinal.entities.Rental;
import com.arnold.dsfinal.entities.User;
import com.arnold.dsfinal.services.ICarService;
import com.arnold.dsfinal.services.IRentalService;
import com.arnold.dsfinal.services.IUserService;

@Controller
public class RentalController {
	
	@Autowired
	private IRentalService rentalService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ICarService carService;
	
//-------------------Retrieve All Users--------------------------------------------------------
    
    @RequestMapping(value = "/rental/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = userService.findAllUsers();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
  //-------------------Retrieve All Cars--------------------------------------------------------
    @RequestMapping(value = "/rental/car/", method = RequestMethod.GET)
    public ResponseEntity<List<Car>> listAllCars() {
        List<Car> cars = carService.findAllCars();
        if(cars.isEmpty()){
            return new ResponseEntity<List<Car>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Car>>(cars, HttpStatus.OK);
    }
    
//-------------------Retrieve All Rentals--------------------------------------------------------
    
    @RequestMapping(value = "/rental/", method = RequestMethod.GET)
    public ResponseEntity<List<Rental>> listAllRentals() {
        List<Rental> rentals = rentalService.findAllRentals();
        if(rentals.isEmpty()){
            return new ResponseEntity<List<Rental>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Rental>>(rentals, HttpStatus.OK);
    }
  //-------------------Create a Rental--------------------------------------------------------
    
    @RequestMapping(value = "/rental/", method = RequestMethod.POST)
    public ResponseEntity<Void> createRental(@RequestBody Rental rental,    UriComponentsBuilder ucBuilder) {
  
        if (rentalService.findById(rental.getId())!=null) {
            System.out.println("A Rental with name " + rental.getId() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
  
        rentalService.saveRental(rental);
  
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rental/{id}").buildAndExpand(rental.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }     
    //------------------- Delete a Rental --------------------------------------------------------
      
    @RequestMapping(value = "/rental/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Rental> deleteRental(@PathVariable("id") int id) {
        System.out.println("Fetching & Deleting Rental with id " + id);
  
        Rental rental = rentalService.findById(id);
        if (rental == null) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<Rental>(HttpStatus.NOT_FOUND);
        }
  
        rentalService.deleteRental(id);
        return new ResponseEntity<Rental>(HttpStatus.NO_CONTENT);
    }

}
