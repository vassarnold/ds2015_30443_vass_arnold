package com.arnold.dsfinal.services;

import java.util.List;

import com.arnold.dsfinal.entities.Car;

public interface ICarService {
	Car findById(int id);
	
	void saveCar(Car car);
	
	void updateCar(Car car);
	
	void deleteCar(int id);

	List<Car> findAllCars(); 
	
	Car findCarByBrand(String brand);
}
