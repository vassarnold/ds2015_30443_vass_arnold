package com.arnold.dsfinal.dao;

import java.util.List;

import com.arnold.dsfinal.entities.Rental;

public interface IRentalDao {
	
	Rental findById(int id);
	
	void saveRental(Rental rental);
	
	void deleteRental(int id);
	
	List<Rental> findAllRentals();
	
	

}
