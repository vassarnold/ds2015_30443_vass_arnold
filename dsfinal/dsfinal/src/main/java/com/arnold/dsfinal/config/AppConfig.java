package com.arnold.dsfinal.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.arnold.dsfinal")
public class AppConfig {

}
