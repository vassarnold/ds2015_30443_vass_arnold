'use strict';
 
var App = angular.module('myApp',[]);
//Declare app level module which depends on filters, and services
App.config(['$routeProvider', '$locationProvider', function($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login',
            controller: LoginController
        });
        $routeProvider.when('/user', {
            templateUrl: 'user',
            controller: UserController
        });
        $routeProvider.otherwise({redirectTo: '/login'});
    }]);
App.run(function($rootScope, $location) {
	    // register listener to watch route changes
	    $rootScope.$on("$routeChangeStart", function(event, next, current) {
	        console.log("Routechanged sessionId="+$rootScope.SessionId);
	        if ($rootScope.SessionId == '' || $rootScope.SessionId == null) {
	            // no logged user, we should be going to #login
	            if (next.templateUrl == "login") {
	                // already going to #login, no redirect needed
	            } else {
	                // not going to #login, we should redirect now
	                $location.path("/login");
	            }
	        }
	    });
	});
