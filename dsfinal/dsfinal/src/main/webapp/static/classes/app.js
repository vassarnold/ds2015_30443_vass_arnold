
'use strict';

var App = angular.module('myApp',['ngRoute']);
App .config(['$routeProvider',
                   function($routeProvider) {
                     $routeProvider.
                       when('/', {
                         templateUrl: '/WEB-INF/views/LoginManagement.jsp',
                         controller: 'LoginController'
                       }).
                       when('/user', {
                         templateUrl: '/WEB-INF/views/UserManagement.jsp',
                         controller: 'UserController'
                       }).
                       when('/car', {
                           templateUrl: '/WEB-INF/views/CarManagement.jsp',
                           controller: 'CarController'
                         }).
                       otherwise({
                         redirectTo: '/login'
                       });
                   }]);