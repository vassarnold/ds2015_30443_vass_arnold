'use strict';

App.factory('CarService', ['$http', '$q', function($http, $q){

	return {
		
			fetchAllCars: function() {
					return $http.get('http://localhost:8080/dsfinal/car/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching cars');
										return $q.reject(errResponse);
									}
							);
			},
		    
		    createCar: function(car){
					return $http.post('http://localhost:8080/dsfinal/car/', car)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating Car');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateCar: function(car, id){
					return $http.put('http://localhost:8080/dsfinal/car/'+id, car)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating Car');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteCar: function(id){
					return $http.delete('http://localhost:8080/dsfinal/car/'+id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting Car');
										return $q.reject(errResponse);
									}
							);
			}

			

	};

}]);
