'use strict';

App.factory('RentalService', ['$http', '$q', function($http, $q){

	return {
		
			fetchAllUsers: function() {
					return $http.get('http://localhost:8080/dsfinal/rental/user')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching users');
										return $q.reject(errResponse);
									}
							);
			},
			fetchAllCars: function() {
				return $http.get('http://localhost:8080/dsfinal/rental/car/')
						.then(
								function(response){
									return response.data;
								}, 
								function(errResponse){
									console.error('Error while fetching cars');
									return $q.reject(errResponse);
								}
						);
		},
		fetchAllRentals: function() {
			return $http.get('http://localhost:8080/dsfinal/rental/')
					.then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while fetching rentals');
								return $q.reject(errResponse);
							}
					);
	},
		createRental: function(rental){
			return $http.post('http://localhost:8080/dsfinal/rental/', rental)
					.then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while creating rental');
								return $q.reject(errResponse);
							}
					);
    },
    deleteRental: function(id){
		return $http.delete('http://localhost:8080/dsfinal/rental/'+id)
				.then(
						function(response){
							return response.data;
						}, 
						function(errResponse){
							console.error('Error while deleting rental');
							return $q.reject(errResponse);
						}
				);
}

	};

}]);
