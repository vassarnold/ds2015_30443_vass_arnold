package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Arnold on 12/11/15.
 */
public interface IMessage extends Remote {

    void sayHello(String name) throws RemoteException;

}
