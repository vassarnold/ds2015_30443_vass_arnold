package views;

import entities.Car;
import rmi.IMessage;
import rmi.IPriceSellingService;
import rmi.ITaxService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DecimalFormat;

/**
 * Created by Arnold on 15/11/15.
 */
public class CarView {
    private JTextField carYearText;
    private JTextField carEngineText;
    private JTextField carPriceText;
    private JLabel carYearLabel;
    private JLabel carEngineLabel;
    private JLabel carPurchasedPriceLabel;
    private JTextArea outputTextArea;
    private JButton buttonDislplay;
    private JPanel carPanel;

    public CarView() {


        buttonDislplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // fire to localhost port 1099
                    Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1099);

                    // search for myMessage service
                    IMessage impl = (IMessage) myRegistry.lookup("myMessage");
                    ITaxService taxService = (ITaxService) myRegistry.lookup("taxService");
                    IPriceSellingService priceSellingService = (IPriceSellingService) myRegistry.lookup("priceSellingService");
                    String carYear = getCarYearText().getText();
                    String carPrice = getCarPriceText().getText();
                    String carEngine = getCarEngineText().getText();
                    DecimalFormat decim = new DecimalFormat("#.##");
                    if (getCarYearText() != null && getCarPriceText() != null && getCarEngineText() != null) {
                        setOutputTextArea("Tax value 1: " + taxService.computeTax(new Car(Integer.parseInt(carYear), Integer.parseInt(carEngine)))
                        +"\nSelling price: " + decim.format(priceSellingService.computePrice(new Car(Integer.parseInt(carYear), Double.parseDouble((carPrice)))))
                        );

                    } else {
                        JOptionPane.showMessageDialog(null, "The fields are empty");
                    }

                    // call server's method
                    System.out.println("Tax value 1: " + taxService.computeTax(new Car(2009, 2000)));
                    System.out.println("Selling price: " + priceSellingService.computePrice(new Car(2009, 5500.0)));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }


        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CarView");
        frame.setContentPane(new CarView().carPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        carPanel = new JPanel();
        carYearLabel = new JLabel();
        carEngineLabel = new JLabel();
        carPurchasedPriceLabel = new JLabel();
        carYearText = new JTextField();
        carEngineText = new JTextField();
        carPriceText = new JTextField();
        buttonDislplay = new JButton();
        outputTextArea = new JTextArea();

    }


    public JTextField getCarYearText() {
        return carYearText;
    }

    public void setCarYearText(JTextField carYearText) {
        this.carYearText = carYearText;
    }

    public JTextField getCarEngineText() {
        return carEngineText;
    }

    public void setCarEngineText(JTextField carEngineText) {
        this.carEngineText = carEngineText;
    }

    public JTextField getCarPriceText() {
        return carPriceText;
    }

    public void setCarPriceText(JTextField carPriceText) {
        this.carPriceText = carPriceText;
    }

    public JTextArea getOutputTextArea() {
        return outputTextArea;
    }

    public void setOutputTextArea(String outputTextArea) {
        this.outputTextArea.setText(outputTextArea);
    }

    public JButton getButtonDislplay() {
        return buttonDislplay;
    }

    public void setButtonDislplay(JButton buttonDislplay) {
        this.buttonDislplay = buttonDislplay;
    }
}
