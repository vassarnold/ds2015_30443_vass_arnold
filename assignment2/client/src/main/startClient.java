package main;

import entities.Car;
import rmi.IMessage;
import rmi.IPriceSellingService;
import rmi.ITaxService;
import views.CarView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Arnold on 12/11/15.
 */
public class startClient {
    public static void main(String[] args) {
        startClient main = new startClient();
        main.doTest();
    }

    private void doTest() {
        try {
            // fire to localhost port 1099
            Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1099);

            // search for myMessage service
            IMessage impl = (IMessage) myRegistry.lookup("myMessage");
            final ITaxService taxService = (ITaxService) myRegistry.lookup("taxService");
            final IPriceSellingService priceSellingService = (IPriceSellingService) myRegistry.lookup("priceSellingService");
            final CarView carView = new CarView();

            // call server's method
            System.out.println("Tax value 1: " + taxService.computeTax(new Car(2009, 2000)));
            System.out.println("Tax value 2: " + taxService.computeTax(new Car(2009, 100)));
            System.out.println("Selling price: " + priceSellingService.computePrice(new Car(2010, 5500.0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


