package main;

import rmi.MessageImpl;
import rmi.PriceSellingService;
import rmi.TaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Arnold on 12/11/15.
 */
public class startServer {
    public static void main(String[] args) {
        startServer main = new startServer();
        main.startServer();
    }

    private void startServer() {
        try {
            // create on port 1099
            Registry registry = LocateRegistry.createRegistry(1099);

            // create a new service named myMessage
            registry.rebind("myMessage", new MessageImpl());
            registry.rebind("taxService", new TaxService());
            registry.rebind("priceSellingService", new PriceSellingService());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("system is ready");
    }
}
