package entities;

import java.io.Serializable;

/**
 * Created by Arnold on 13/11/15.
 */
public class Car implements Serializable {
    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private double pricePurchased;

    public Car() {
    }

    public Car(int year, int engineCapacity) {
        this.year = year;
        this.engineCapacity = engineCapacity;
    }

    public Car(int year, double pricePurchased) {
        this.year = year;
        this.pricePurchased = pricePurchased;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getPricePurchased() {
        return pricePurchased;
    }

    public void setPricePurchased(double pricePurchased) {
        this.pricePurchased = pricePurchased;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (engineCapacity != car.engineCapacity) return false;
        if (Double.compare(car.pricePurchased, pricePurchased) != 0) return false;
        if (year != car.year) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = year;
        result = 31 * result + engineCapacity;
        temp = Double.doubleToLongBits(pricePurchased);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "year=" + year +
                ", engineCapacity=" + engineCapacity +
                ", pricePurchased=" + pricePurchased +
                '}';
    }
}
