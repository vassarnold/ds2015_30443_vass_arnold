package rmi;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Arnold on 13/11/15.
 */
public interface IPriceSellingService extends Remote {
    double computePrice(Car car) throws RemoteException;
}
