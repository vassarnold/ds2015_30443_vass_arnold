package rmi;

import entities.Car;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;

/**
 * Created by Arnold on 13/11/15.
 */
public class PriceSellingService extends UnicastRemoteObject implements IPriceSellingService {
    public PriceSellingService() throws RemoteException {
    }

    @Override
    public double computePrice(Car car) throws RemoteException {
        if (car.getPricePurchased() <= 0) {
            throw new IllegalArgumentException("Price must be positive.");
        }
        double priceSelling = car.getPricePurchased() - (car.getPricePurchased()/7.0)*( Calendar.getInstance().get(Calendar.YEAR) - car.getYear() );
        return priceSelling ;
    }
}
