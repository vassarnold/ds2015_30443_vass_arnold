package rmi;

import entities.Car;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Arnold on 13/11/15.
 */
public class TaxService extends UnicastRemoteObject implements ITaxService {
    public TaxService() throws RemoteException {
    }

    @Override
    public double computeTax(Car car) throws RemoteException {
// Dummy formula
        if (car.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(car.getEngineCapacity() > 1601) sum = 18;
        if(car.getEngineCapacity() > 2001) sum = 72;
        if(car.getEngineCapacity() > 2601) sum = 144;
        if(car.getEngineCapacity() > 3001) sum = 290;
        return car.getEngineCapacity() / 200.0 * sum;
    }
}
