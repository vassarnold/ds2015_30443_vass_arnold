package rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Arnold on 12/11/15.
 */
public class MessageImpl extends UnicastRemoteObject implements IMessage {

    public MessageImpl() throws RemoteException {
    }

    @Override
    public void sayHello(String name) throws RemoteException {
        System.out.println("hello "+name);
    }
}
