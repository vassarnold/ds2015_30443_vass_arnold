package rmi;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Arnold on 13/11/15.
 */
public interface ITaxService extends Remote {
    double computeTax(Car car) throws RemoteException;
}
